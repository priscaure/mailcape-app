import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('./components/HomePage.vue')
        },
        {
            path: '/pricing',
            name: 'pricing',
            component: () => import('./components/PricingPage.vue')
        },
        {
            path: '/faq',
            name: 'faq',
            component: () => import('./components/FAQPage.vue')
        },
        {
            path: '/about',
            name: 'about',
            component: () => import('./components/HelloWorld.vue')
            // meta: {
            //     authRequired: true
            // }
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('./components/LoginPage.vue')
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: () => import('./components/DashboardPage.vue')
        },
        {
            path: '/signup',
            name: 'signup',
            component: () => import('./components/SignupPage.vue')
        },
        {
            path: '/logout',
            name: 'logout',
            component: () => import('./components/LogoutPage.vue')
        }
    ],
    pushWithAnchor: function (routeName, toHash) {
        const fromHash = Router.history.current.hash
        fromHash !== toHash || !fromHash
        ? Router.push({ name: routeName, hash: toHash  })
        : Router.push({ name: routeName, hash: fromHash  }, undefined, () => { window.location.href = toHash  })
      }
});

// router.beforeEach((to, from, next) => {
//     if (to.matched.some(record => record.meta.authRequired)) {
//         if (!store.state.isAuthenticated) {
//             next({
//                 path: '/sign-in'
//             });
//         } else {
//             next();
//         }
//     } else {
//         next();
//     }
// });

export default router;
