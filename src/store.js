import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        status: '',
        token: localStorage.getItem('token') || '',
        // token: '',
        user_firstname : localStorage.getItem('user_firstname') || '',
        dst_email : localStorage.getItem('dst_email') ||'',
        user_id : localStorage.getItem('user_id') || null,
        is_admin: localStorage.getItem('is_admin') || false
    },
    mutations: {
        auth_request(state){
            state.status = 'loading'
        },
        auth_success(state, user_info){
            state.status = 'success'
            state.token = user_info.token
            localStorage.setItem('token', user_info.token)
            state.user_firstname = user_info.first_name
            localStorage.setItem('user_firstname', user_info.first_name)
            state.user_id = user_info.id
            localStorage.setItem('user_id', user_info.id)
            state.dst_email = user_info.dst_email
            localStorage.setItem('dst_email', user_info.dst_email)
            state.is_admin = user_info.is_admin
            localStorage.setItem('is_admin', user_info.is_admin)
        },
        auth_error(state){
            state.status = 'error'
        },
        logout(state){
            state.status = ''
            state.token = ''
            state.user_firstname = ''
            state.user_id = null
            state.dst_email = ''
            state.is_admin = false
            localStorage.removeItem('token')
            localStorage.clear()
        },

    },
    actions: {
        login({commit}, user){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios({url: 'https://api.mailcape.com/login_check', data: user, method: 'POST' })
                    .then(resp => {
                        axios.defaults.headers.common['Authorization'] = resp.data.token
                        commit('auth_success', { token: resp.data.token, first_name: resp.data.user, id: resp.data.user_id, is_admin: resp.data.is_admin, dst_email: resp.data.email })
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        localStorage.removeItem('token')
                        reject(err)
                    })
            })
        },
        logout({commit}){
            return new Promise((resolve, reject) => {
                commit('logout')
                delete axios.defaults.headers.common['Authorization']
                resolve().catch(err => { reject(err) })
            })
        }

    },
    getters : {
        isLoggedIn: state => !!state.token,
        isAdmin: state => state.is_admin,
        authStatus: state => state.status,
        userName: state => state.user_firstname,
        token: state => state.token
    }
})
